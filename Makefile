BINARY = hpatten
DEVICE = STM32F401CBU6
OBJS += hpatten.o
CFLAGS += -O2 -ggdb3
CXXFLAGS += -std=c++11
CPPFLAGS += -MD
LDFLAGS += -static -nostartfiles
LDLIBS += -Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group

include $(OPENCM3_DIR)/mk/genlink-config.mk
include $(OPENCM3_DIR)/mk/gcc-config.mk

.PHONY: clean all size

all: $(BINARY).elf size

size: $(BINARY).elf
	size $(BINARY).elf

flash: $(BINARY).elf
	openocd -f flash.cfg

clean:
	rm -rf *.elf *.hex *.o *.d *~ *.ld .gdb_history

include $(OPENCM3_DIR)/mk/genlink-rules.mk
include $(OPENCM3_DIR)/mk/gcc-rules.mk
