#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/flash.h>

#include <libopencm3/cm3/assert.h>
#include <libopencm3/stm32/pwr.h>
#include <libopencm3/usb/usbd.h>

#include <assert.h>
#include <stdlib.h>
#include <string.h>

// Copyright (C) 2009 Federico Ruiz-Ugalde <memeruiz at gmail dot com>
// Copyright (C) 2009 Uwe Hermann <uwe@hermann-uwe.de>
// Copyright (C) 2010 Thomas Otto <tommi@viadmin.org>
// Copyright (C) 2015 Karl Palsson <karlp@tweak.net.au>
// Copyright (C) 2017 Harmon Instruments, LLC

volatile uint8_t state_desired = 0;
uint8_t state_current = 0x0F;

#define BULK_EP_MAXPACKET       64

static const char *usb_strings[] = {
        "Harmon Instruments, LLC",
        "Step Attenuator Driver",
        "0.01",
        "data"
};

static const struct usb_device_descriptor dev = {
	.bLength = USB_DT_DEVICE_SIZE,
	.bDescriptorType = USB_DT_DEVICE,
        .bcdUSB = 0x0200,
        .bDeviceClass = USB_CLASS_VENDOR,
	.bDeviceSubClass = 0,
        .bDeviceProtocol = 0,
        .bMaxPacketSize0 = BULK_EP_MAXPACKET,
        .idVendor = 0xcafe,
        .idProduct = 0xcafe,
        .bcdDevice = 0x0001,
        .iManufacturer = 1,
	.iProduct = 2,
        .iSerialNumber = 3,
        .bNumConfigurations = 1,
};

static const struct usb_endpoint_descriptor endp_bulk[] = {
        {
                .bLength = USB_DT_ENDPOINT_SIZE,
                .bDescriptorType = USB_DT_ENDPOINT,
                .bEndpointAddress = 0x01,
                .bmAttributes = USB_ENDPOINT_ATTR_BULK,
                .wMaxPacketSize = BULK_EP_MAXPACKET,
                .bInterval = 1,
        },
        {
                .bLength = USB_DT_ENDPOINT_SIZE,
                .bDescriptorType = USB_DT_ENDPOINT,
                .bEndpointAddress = 0x82,
                .bmAttributes = USB_ENDPOINT_ATTR_BULK,
                .wMaxPacketSize = BULK_EP_MAXPACKET,
                .bInterval = 1,
        }
};

static const struct usb_interface_descriptor iface_sourcesink[] = {
        {
                .bLength = USB_DT_INTERFACE_SIZE,
                .bDescriptorType = USB_DT_INTERFACE,
                .bInterfaceNumber = 0,
                .bAlternateSetting = 0,
                .bNumEndpoints = 2,
                .bInterfaceClass = USB_CLASS_VENDOR,
                .iInterface = 0,
                .endpoint = endp_bulk,
        }
};

static const struct usb_interface ifaces_sourcesink[] = {
        {
	num_altsetting : 1,
	altsetting : iface_sourcesink
        }
};

static const struct usb_config_descriptor config[] = {
        {
                .bLength = USB_DT_CONFIGURATION_SIZE,
                .bDescriptorType = USB_DT_CONFIGURATION,
                .wTotalLength = 0,
                .bNumInterfaces = 1,
                .bConfigurationValue = 1,
                .iConfiguration = 4, /* string index */
                .bmAttributes = 0x80,
                .bMaxPower = 200, // 2 mA increment
                .interface = ifaces_sourcesink,
        }
};

/* Buffer to be used for control requests. */
static uint8_t usbd_control_buffer[2*BULK_EP_MAXPACKET];

static void gadget0_ss_out_cb(usbd_device *usbd_dev, uint8_t ep)
{
        uint8_t buf[BULK_EP_MAXPACKET] __attribute__ ((aligned(4)));
	uint16_t x = usbd_ep_read_packet(usbd_dev, ep, buf, BULK_EP_MAXPACKET);
	if(x>0)
                state_desired = buf[0];

}

static void gadget0_ss_in_cb(usbd_device *usbd_dev, uint8_t ep)
{
 	(void) usbd_dev;
        uint8_t buf[BULK_EP_MAXPACKET + 1] __attribute__ ((aligned(4)));
	buf[0] = state_current;
	uint16_t x = usbd_ep_write_packet(usbd_dev, ep, buf, BULK_EP_MAXPACKET);
	/* As we are calling write in the callback, this should never fail */
	if (x != BULK_EP_MAXPACKET) {
	}
}

static void gadget0_set_config(usbd_device *usbd_dev, uint16_t wValue)
{
        usbd_ep_setup(usbd_dev, 0x01, USB_ENDPOINT_ATTR_BULK, BULK_EP_MAXPACKET,
                      gadget0_ss_out_cb);
        usbd_ep_setup(usbd_dev, 0x82, USB_ENDPOINT_ATTR_BULK, BULK_EP_MAXPACKET,
                      gadget0_ss_in_cb);
}

static usbd_device *our_dev;

int main(void)
{
	/* Enable internal high-speed oscillator. */
        //rcc_osc_on(RCC_HSI);
	RCC_CR |= RCC_CR_HSION;
        rcc_wait_for_osc_ready(RCC_HSI);

        /* Select HSI as SYSCLK source. */
        rcc_set_sysclk_source(RCC_CFGR_SW_HSI);

        /* Enable external high-speed oscillator */
	RCC_CR |= RCC_CR_HSEON;//rcc_osc_on(RCC_HSE);
        rcc_wait_for_osc_ready(RCC_HSE);

        /* Enable/disable high performance mode */
	pwr_set_vos_scale(PWR_SCALE1);
	//pwr_set_vos_scale(PWR_SCALE2);

        /*
         * Set prescalers for AHB, ADC, ABP1, ABP2.
         * Do this before touching the PLL
         */
        rcc_set_hpre(RCC_CFGR_HPRE_DIV_NONE);
        rcc_set_ppre1(RCC_CFGR_PPRE_DIV_2);
        rcc_set_ppre2(RCC_CFGR_PPRE_DIV_NONE);

	rcc_set_main_pll_hse(
		24, // PLLM, R divider, input freq in MHz
		336, // PLLN, VCO freq in MHz
		4, // PLLP, CPU divider
		7, // PLLQ, USB divider
                0 // PLLR
	       );

        /* Enable PLL oscillator and wait for it to stabilize. */
	RCC_CR |= RCC_CR_PLLON;
	//rcc_osc_on(RCC_PLL);
        rcc_wait_for_osc_ready(RCC_PLL);

        /* Configure flash settings. */
        flash_set_ws(FLASH_ACR_ICEN | FLASH_ACR_DCEN | FLASH_ACR_LATENCY_2WS);

        /* Select PLL as SYSCLK source. */
        rcc_set_sysclk_source(RCC_CFGR_SW_PLL);

	/* Wait for PLL clock to be selected. */
        rcc_wait_for_sysclk_status(RCC_PLL);

        /* Set the peripheral clock frequencies used. */
        rcc_ahb_frequency  = 84000000;
	rcc_apb1_frequency = 42000000;
        rcc_apb2_frequency = 84000000;

        /* Disable internal high-speed oscillator. */
	//rcc_osc_off(RCC_HSI);
	RCC_CR &= ~RCC_CR_HSION;

	// Init peripherals
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_GPIOD);
	gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO0 | GPIO1);
	rcc_periph_clock_enable(RCC_OTGFS);

	// FETs
	gpio_clear(GPIOA, 0xFF);
	gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, 0xFF);


	// USB
        gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE,
			GPIO9 | GPIO11 | GPIO12);
        gpio_set_af(GPIOA, GPIO_AF10, GPIO9 | GPIO11 | GPIO12);

	// LED
	gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT,
			GPIO_PUPD_NONE, GPIO14);
	gpio_set(GPIOB, GPIO14);

	our_dev = usbd_init(&otgfs_usb_driver, &dev, config,
			    usb_strings, 5,
			    usbd_control_buffer, sizeof(usbd_control_buffer));

	// Timer 1 kHz interrupt
	rcc_periph_clock_enable(RCC_TIM2);
	rcc_periph_reset_pulse(RST_TIM2);
	timer_set_prescaler(TIM2, 8399); // (84 MHz / 10khz) - 1
	timer_set_period(TIM2, 9); // (10 kHz / 1 kHz) - 1
	nvic_enable_irq(NVIC_TIM2_IRQ);
	timer_enable_update_event(TIM2);
	timer_enable_irq(TIM2, TIM_DIER_UIE);
	timer_enable_counter(TIM2);

        usbd_register_set_config_callback(our_dev, gadget0_set_config);

	while (1) {
		usbd_poll(our_dev);
	}
	return 0;
}

uint8_t ledcount = 0;
uint8_t ms_since_last_toggle = 0;
void tim2_isr(void)
{
	TIM_SR(TIM2) &= ~TIM_SR_UIF;
	if (ledcount++ == 249)
	{
		ledcount = 0;
		gpio_toggle(GPIOB, GPIO14);
	}
	if(ms_since_last_toggle > 19)
	{
		gpio_clear(GPIOA, 0xFF);
		for(int i=0; i<4; i++)
		{
			int bit_current = 0x1 & (state_current >> i);
			int bit_desired = 0x1 & (state_desired >> i);
			if(bit_current == bit_desired)
				continue;
			if(bit_desired == 0)
			{
				gpio_set(GPIOA, (1<<(2*i)));
				state_current &= ~(1<<i);
			}
			else
			{
				gpio_set(GPIOA, (1<<(2*i+1)));
				state_current |= (1<<i);
			}
			ms_since_last_toggle = 0;
			break;
		}
	}
	else
	{
		ms_since_last_toggle++;
	}
}
