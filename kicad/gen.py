#!/usr/bin/env python

from nosch import *

add('U1', 'STM32F401', 'QFN48_7x7', pins = [
    [2, 'LEDO', 'PC13'], # EVENTOUT
    [3, '', 'PC14'], # EVENTOUT, OSC32_IN
    [4, '', 'PC15'], # EVENTOUT, OSC32_OUT
    [5, 'OSCI', 'PHO'], # OSC IN
    [6, 'OSCO', 'PH1'], # OSC OUT
    [10, 'PA0', 'PA0'], # ADC1_0, WKUP, TIM2_CH1, TIM5_CH1, EVENTOUT
    [11, 'PA1', 'PA1'], # ADC1_1, TIM2_CH2, TIM5_CH2, EVENTOUT
    [12, 'PA2', 'PA2'], # ADC1_2, TIM2_CH3, USART2_TX, EVENTOUT
    [13, 'PA3', 'PA3'], # ADC1_3, TIM2_CH4, USART2_RX, EVENTOUT
    [14, 'PA4', 'PA4'], # ADC1_4, SPI1_NSS, SPI3_NSS
    [15, 'PA5', 'PA5'], # ADC1_5, SPI1_SCK, TIM2_CH1
    [16, 'PA6', 'PA6'], # ADC1_6, SPI1_MISO,
    [17, 'PA7', 'PA7'], # ADC1_7, SPI1_MOSI,
    [18, '', 'PB0'], # ADC1_8
    [19, '', 'PB1'], # ADC1_9
    [21, '', 'PB10'], # SPI2_SCK, I2C2_SCL
    [25, '', 'PB12'], # SPI2_NSS, I2C2_SMBA
    [26, '', 'PB13'], # SPI2_SCK
    [27, 'LED0', 'PB14'], # SPI2_MISO
    [28, '', 'PB15'], # SPI2_MOSI
    [29, '', 'PA8'], # I2C3_SCL, MCO_2
    [30, '3V3', 'PA9'], # USB_VBUS I2C3_SMBA, USART1_TX
    [31, 'ID', 'PA10'], # USART1_RX, USB_FS_ID
    [32, 'DM1', 'PA11'], # USB_DM
    [33, 'DP1', 'PA12'], # USB_DP
    [34, 'SWDIO', 'JTMS_SWDIO'],
    [35, g, 'VSS'],
    [36, '3V3', 'VDD'],
    [37, 'SWCLK', 'JTCK_SWCLK'],
    [38, '', 'JTDI'],
    [39, '', 'JTDO_SWO'], # SPI1_SCK, I2C2_SDA
    [40, '', 'PB4/NJRST'], # SPI1_MISO, I2C3 SDA
    [41, '', 'PB5'], # SPI_MOSI
    [42, '', 'PB6'], # I2C1_SCL, USART1_TX
    [43, '', 'PB7'], # I2C1_SDA, USART1_RX
    [45, '', 'PB8'], # I2C1_SCL,
    [46, '', 'PB9'], # I2C1_SDA,
    [44, 'BOOT0', 'BOOT0'], # 0 for main flash, 1 for ROM/SRAM
    [20, g, 'BOOT1'], # PB2, 0 for ROM, 1 for SRAM
    [22, 'VCAP', 'VCAP1'], # 1.2 V, needs 4.7 uF
    [1, '3V3', 'VBAT'],
    [7, 'NRST', 'NRST'],
    [9, '3V3', 'VDDA'],
    ['8 23 47 49', g, 'VSS'],
    ['24 48', '3V3', 'VDD']])

# ESD prot.
add('U2', '8QKYD', 'SOT23-6', pins = [
    ['1 6', ['DP1', 'DP0'], 'IO1'],
    ['3 4', ['DM1', 'DM0'], 'IO2'],
    [5, 'VBUS', 'VBUS'],
    [2, g]])

# USB
add('J1', 'JVWW9', 'USB_micro_B', pins = [
    [1, 'VBUS'], [2, 'DM0'], [3, 'DP0'], [4, 'ID', 'ID'], [5, g]])

c(10, '4.7u', 'VCAP', g, fp='0805')
c(11, c100n, 'NRST', g)

# oscillator
add('U3', 'K7CM9', 'osc_2.5x2', pins=[['1 4', '3V3'], [2, g], [3, 'OSCI']])

r(1, r1k, 'BOOT0', g)
led_r(2, rv=r1k, p='LED0')

for i in range(8):
    irfml8244(100+i, g='PA{}'.format(i), d='Q{}'.format(i), s=g)
    r(100+i, r5k, 'PA{}'.format(i), g)

add('U100', 'ESDA19SC6', 'SOT23-6', pins=[['1 3 4 6', ['Q0', 'Q1', 'Q2', 'Q3']], ['2 5', g]])
add('U101', 'ESDA19SC6', 'SOT23-6', pins=[['1 3 4 6', ['Q4', 'Q5', 'Q6', 'Q7']], ['2 5', g]])

add('P2', 'DF13_10', 'DF13_DR_SM_10', [
    [1, 'Q0', 'S1_0'], [2, 'Q1', 'S1_1'],
    [3, g], [4, 'Q4', 'S3_0'],
    [5, 'Q2', 'S2_0'], [6, 'Q6', 'S4_0'],
    [7, 'Q7', 'S4_1'], [8, 'Q3', 'S2_1'],
    [9, 'Q5', 'S3_1'], [10, 'COMM']])

c(900, c10u, 'VBUS', g, fp='0805')
c(100, c10u, 'COMM', g, fp='0805')
# 3.3 V
ldo_sot(940, 'CGK8Y', vi = 'VBUS', vo='3V3')
for i in range(2):
    c(941+i, c100n, '3V3', g)

add('J800', 'DNP', 'TC2030-NL', pins = [[1, '3V3'],
                                        [2, 'SWDIO', 'TMS'],
                                        [3, 'SWCLK', 'TCK'],
                                        [4, '', 'TDO'],
                                        [5, 'NRST'], [6, g]])

pcb.write("hpatten.net")
